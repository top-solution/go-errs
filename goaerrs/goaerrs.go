package goaerrs

import (
	"github.com/goadesign/goa"
	"gitlab.com/top-solution/go-errs"
)

var (
	ErrBadRequest          = goa.ErrBadRequest
	ErrUnauthorized        = goa.ErrUnauthorized
	ErrInvalidRequest      = goa.ErrInvalidRequest
	ErrInvalidEncoding     = goa.ErrInvalidEncoding
	ErrRequestBodyTooLarge = goa.ErrRequestBodyTooLarge
	ErrNoAuthMiddleware    = goa.ErrNoAuthMiddleware
	ErrInvalidFile         = goa.ErrInvalidFile
	ErrNotFound            = goa.ErrNotFound
	ErrMethodNotAllowed    = goa.ErrMethodNotAllowed
	ErrInternal            = goa.ErrInternal
	ErrPreconditionFailed  = goa.NewErrorClass("precondition_failed", 412)
	ErrConflict            = goa.NewErrorClass("conflict", 409)
)

func ToGoa(err error) error {
	if err == nil {
		return nil
	}
	if _, ok := err.(goa.ServiceError); ok {
		return err
	}
	switch {
	case errs.IsAlreadyExists(err):
		return ErrConflict(err)
	case errs.IsBadRequest(err):
		return goa.ErrBadRequest(err)
	case errs.IsNotFound(err):
		return goa.ErrNotFound(err)
	case errs.IsPreconditionFailed(err):
		return ErrPreconditionFailed(err)
	default:
		return goa.ErrInternal(err)
	}
}
