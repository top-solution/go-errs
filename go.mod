module gitlab.com/top-solution/go-errs

go 1.13

require (
	github.com/armon/go-metrics v0.3.0 // indirect
	github.com/dimfeld/httptreemux v5.0.1+incompatible // indirect
	github.com/friendsofgo/errors v0.9.2
	github.com/goadesign/goa v1.4.2
	github.com/gofrs/uuid v3.2.0+incompatible // indirect
)
