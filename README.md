# go-errs

go-errs is a small utility for wrapping and annotating errors using github.com/friendsofgo/errors, providing a GoaV1 middleware to automatically map exported error types to Goa errors

**Note**: in the future github.com/friendsofgo/errors will likely be removed as a dependency, don't count on it

## Using the middleware

1 - Mount the middleware

```go
import "gitlab.com/top-solution/errs/goaerrs"

func main() {
    ...
   	service := goa.New("myapi")
   	service.Use(goaerrs.ErrorHandler(service, true))
}
```

2 - Somewhere, create an error:

```go
    func doStuff() ([]stuff, error) {
        thing, err := fetchThing()
        if thing == nil {
            return nil, errs.NewNotFound(errors.New("thing does not exist"))
        }
        ...
    }
```

or wrap an existing one:

```go
    func doStuff() ([]stuff, error) {
        ...
        things, err := fetchThings()
        if err != nil {
            return nil, errs.NewNotFound(err, "fetch things")
        }
        ...
    }
```

3 - Return the error from your controller

```
func (c *MyStuffController) DoStuff(ctx *app.DoStuffMyStuffContext) error {
    ...
	result, err := doStuff()
	if err != nil {
		return err
	}
    ...
}
```

You should now see the goa-formatted err when calling the API:
```
{"id":"2OLSH9Fb","code":"not_found","status":404,"detail": "thing does not exist"}
```

If you return a generic error, you will get a 500 as a response and a stacktrace in your logs:
```
2019/12/19 10:54:30 [EROR] uncaught error req_id=5gBCPgC2tY-1 req_id=5gBCPgC2tY-1 error.stack=i'm a error
stuff/controllers.(*MyStuffController).DoStuff
        stuff/api/controllers/stuff.go:91
stuff/app.MountMyStuffController.func3
        /home/luca/projects/stuff/api/app/controllers.go:926
    ...
```


