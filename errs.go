package errs

import (
	"strings"

	"github.com/friendsofgo/errors"
	"github.com/goadesign/goa"
)

type NotFound struct {
	error
}

type AlreadyExists struct {
	error
}

type BadRequest struct {
	error
}

type NotProvisioned struct {
	error
}

type PreconditionFailed struct {
	error
}

var ErrPreconditionFailed = goa.NewErrorClass("precondition_failed", 412)

func NewNotFound(err error, message ...string) error {
	return &NotFound{Wrap(err, message...)}
}

func NotFoundf(format string, args ...interface{}) error {
	return &NotFound{errors.Errorf(format, args...)}
}

func IsNotFound(err error) bool {
	var target *NotFound
	return errors.As(err, &target)
}

func NewAlreadyExists(err error, message ...string) error {
	return &AlreadyExists{Wrap(err, message...)}
}

func AlreadyExistsf(format string, args ...interface{}) error {
	return &AlreadyExists{errors.Errorf(format, args...)}
}

func IsAlreadyExists(err error) bool {
	var target *AlreadyExists
	return errors.As(err, &target)
}

func NewBadRequest(err error, message ...string) error {
	return &BadRequest{Wrap(err, message...)}
}

func BadRequestf(format string, args ...interface{}) error {
	return &BadRequest{errors.Errorf(format, args...)}
}

func IsBadRequest(err error) bool {
	var target *BadRequest
	return errors.As(err, &target)
}

func NewNotProvisioned(err error, message ...string) error {
	return &NotProvisioned{Wrap(err, message...)}
}

func NotProvisionedf(format string, args ...interface{}) error {
	return &NotProvisioned{errors.Errorf(format, args...)}
}

func IsNotProvisioned(err error) bool {
	var target *NotProvisioned
	return errors.As(err, &target)
}

func NewPreconditionFailed(err error, message ...string) error {
	return &PreconditionFailed{Wrap(err, message...)}
}

func PreconditionFailedf(format string, args ...interface{}) error {
	return &PreconditionFailed{errors.Errorf(format, args...)}
}

func IsPreconditionFailed(err error) bool {
	var target *PreconditionFailed
	return errors.As(err, &target)
}

func Wrap(err error, message ...string) error {
	if len(message) == 0 {
		return errors.WithStack(err)
	}
	return errors.Wrap(err, strings.Join(message, ","))
}

func Wrapf(err error, format string, args ...interface{}) error {
	return errors.Wrapf(err, format, args...)
}

func New(message string) error {
	return errors.New(message)
}
